# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'DateField'
        db.create_table(u'cms_datefield', (
            (u'field_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cms.Field'], unique=True, primary_key=True)),
            ('value', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal(u'cms', ['DateField'])

        # Adding model 'CharField'
        db.create_table(u'cms_charfield', (
            (u'field_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cms.Field'], unique=True, primary_key=True)),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'cms', ['CharField'])

        # Adding model 'IntField'
        db.create_table(u'cms_intfield', (
            (u'field_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cms.Field'], unique=True, primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'cms', ['IntField'])

        # Adding model 'DateTimeField'
        db.create_table(u'cms_datetimefield', (
            (u'field_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cms.Field'], unique=True, primary_key=True)),
            ('value', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'cms', ['DateTimeField'])

        # Adding field 'Field.document'
        db.add_column(u'cms_field', 'document',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['cms.Document']),
                      keep_default=False)

    def backwards(self, orm):
        # Deleting model 'DateField'
        db.delete_table(u'cms_datefield')

        # Deleting model 'CharField'
        db.delete_table(u'cms_charfield')

        # Deleting model 'IntField'
        db.delete_table(u'cms_intfield')

        # Deleting model 'DateTimeField'
        db.delete_table(u'cms_datetimefield')

        # Deleting field 'Field.document'
        db.delete_column(u'cms_field', 'document_id')


    models = {
        u'cms.charfield': {
            'Meta': {'object_name': 'CharField', '_ormbases': [u'cms.Field']},
            u'field_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['cms.Field']", 'unique': 'True', 'primary_key': 'True'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'cms.datefield': {
            'Meta': {'object_name': 'DateField', '_ormbases': [u'cms.Field']},
            u'field_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['cms.Field']", 'unique': 'True', 'primary_key': 'True'}),
            'value': ('django.db.models.fields.DateField', [], {})
        },
        u'cms.datetimefield': {
            'Meta': {'object_name': 'DateTimeField', '_ormbases': [u'cms.Field']},
            u'field_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['cms.Field']", 'unique': 'True', 'primary_key': 'True'}),
            'value': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'cms.document': {
            'Meta': {'object_name': 'Document'},
            'creation_date': ('django.db.models.fields.DateTimeField', [], {}),
            'document_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cms.DocumentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['cms.Document']"}),
            u'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'update_date': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'cms.documenttype': {
            'Meta': {'object_name': 'DocumentType'},
            'creation_date': ('django.db.models.fields.DateTimeField', [], {}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1500'}),
            'fields': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['cms.FieldType']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'cms.field': {
            'Meta': {'object_name': 'Field'},
            'document': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cms.Document']"}),
            'field_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cms.FieldType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'cms.fieldtype': {
            'Meta': {'object_name': 'FieldType'},
            'field_type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'cms.intfield': {
            'Meta': {'object_name': 'IntField', '_ormbases': [u'cms.Field']},
            u'field_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['cms.Field']", 'unique': 'True', 'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['cms']
