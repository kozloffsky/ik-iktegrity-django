from django.contrib import admin
from cms.models import Document, DocumentType, FieldType
# Register your models here.
admin.site.register(Document)
admin.site.register(DocumentType)
admin.site.register(FieldType)
