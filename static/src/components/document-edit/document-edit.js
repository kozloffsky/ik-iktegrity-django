define(['knockout', 'app/models/model', 'text!./document-edit.html'], function(ko, model, templateMarkup) {

    function DocumentEdit(params) {
	this.message = ko.observable('Hello from the document-edit component!');
	this.documentTypes = model.factory('DocumentType').all();
	this.documentFields = ko.observableArray();
    }

    DocumentEdit.prototype.selectedTypeChanged = function(data, event){
	console.log(event.currentTarget.value);
	this.documentFields.removeAll();
	
    }

    DocumentEdit.prototype.dispose = function() { };
    
    return { viewModel: DocumentEdit, template: templateMarkup };

});
