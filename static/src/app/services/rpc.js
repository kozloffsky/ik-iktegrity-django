define(['knockout','jquery','vendor/jquery.jsonrpcclient'], function(ko, j, rpc){
    return (function(){
	function Client(){
	    this.rpc = new j.JsonRpcClient({ 
		ajaxUrl: "http://127.0.0.1:8000/api/"
	    });
	    console.log(this.rpc);
	}

	Client.prototype.call = function(method, params){
	    var deferred = j.Deferred();
	    this.rpc.call(method, params, 
			  //SUCESS CALLBACK
			  function(result){
			      deferred.resolve(result);
			  },
			  //ERROR CALLBACK
			  function(error){
			      deferred.reject(error);
			  });

	    return deferred.promise();			  
	}

	return new Client();
    })()
});
