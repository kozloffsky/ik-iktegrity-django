define(['knockout','jquery', 'app/services/rpc'], function(ko, j, rpc){
    /**
     * Models Factory - create wrapper for remote model
     **/
     function factory(remoteName){
	this.remoteName = remoteName;
	
	var Model = function(){
	}
	
	Model.all = function(){
	    var objects = ko.observableArray([]);
	    j.when(rpc.call("cms.findAll", [remoteName]))
		.then(function(data){
		    data = j.parseJSON(data);
		    for(var i in data){
			remoteModel = data[i];
			var model = new Model();
			model.pk = remoteModel.pk;
			model.setFields(remoteModel.fields);
			objects.push(model);
		    }

		});
	    return objects;
	}

	 var $fields;
	 Model.prototype.setFields = function(fields){
	     $fields = fields;
	     for(var field in $fields){
		 this[field] = ko.observable($fields[field]);
	     }
     	     console.log(this.fields());
	 }

	return Model;
    }

    return {
	factory: factory
    }
});
