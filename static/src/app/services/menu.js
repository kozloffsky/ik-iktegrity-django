define(['knockout','jquery','app/services/rpc'], function(ko, j, rpc){
    return (function(){
	var items = ko.observableArray([]);
	 var MenuItem = function(label, route)
	{
	    this.label = label;
	    this.route = route;
	}
 
	
	function  MenuService(){
	    console.log("sdfsdf")
	}

	MenuService.prototype.getItems = function(){
	    j.when(rpc.call('cms.getMenuItems',[])).then(
		function(result){
		    for (var r in result){
			var route = result[r];
			items.push(new MenuItem(route.label, route.route));
		    }
		}, 
		function(error){
		    console.log("ERROR!!!", error);
		});
	    return items;
	}

	return new MenuService();
    })(); 
});
