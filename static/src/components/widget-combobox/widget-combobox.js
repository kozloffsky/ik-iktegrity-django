define(['knockout', 'text!./widget-combobox.html'], function(ko, templateMarkup) {

  function WidgetCombobox(params) {
    this.message = ko.observable('Hello from the widget-combobox component!');
  }

  // This runs when the component is torn down. Put here any logic necessary to clean up,
  // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
  WidgetCombobox.prototype.dispose = function() { };
  
  return { viewModel: WidgetCombobox, template: templateMarkup };

});
