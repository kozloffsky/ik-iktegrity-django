# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Document.parent_document'
        db.delete_column(u'cms_document', 'parent_document_id')

        # Adding field 'Document.parent'
        db.add_column(u'cms_document', 'parent',
                      self.gf('mptt.fields.TreeForeignKey')(blank=True, related_name='children', null=True, to=orm['cms.Document']),
                      keep_default=False)

        # Adding field 'Document.lft'
        db.add_column(u'cms_document', u'lft',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0, db_index=True),
                      keep_default=False)

        # Adding field 'Document.rght'
        db.add_column(u'cms_document', u'rght',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0, db_index=True),
                      keep_default=False)

        # Adding field 'Document.tree_id'
        db.add_column(u'cms_document', u'tree_id',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0, db_index=True),
                      keep_default=False)

        # Adding field 'Document.level'
        db.add_column(u'cms_document', u'level',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0, db_index=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Document.parent_document'
        db.add_column(u'cms_document', 'parent_document',
                      self.gf('mptt.fields.TreeForeignKey')(related_name='children', null=True, to=orm['cms.Document'], blank=True),
                      keep_default=False)

        # Deleting field 'Document.parent'
        db.delete_column(u'cms_document', 'parent_id')

        # Deleting field 'Document.lft'
        db.delete_column(u'cms_document', u'lft')

        # Deleting field 'Document.rght'
        db.delete_column(u'cms_document', u'rght')

        # Deleting field 'Document.tree_id'
        db.delete_column(u'cms_document', u'tree_id')

        # Deleting field 'Document.level'
        db.delete_column(u'cms_document', u'level')


    models = {
        u'cms.document': {
            'Meta': {'object_name': 'Document'},
            'creation_date': ('django.db.models.fields.DateTimeField', [], {}),
            'document_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cms.DocumentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['cms.Document']"}),
            u'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'update_date': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'cms.documenttype': {
            'Meta': {'object_name': 'DocumentType'},
            'creation_date': ('django.db.models.fields.DateTimeField', [], {}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1500'}),
            'fields': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['cms.FieldType']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'cms.field': {
            'Meta': {'object_name': 'Field'},
            'field_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cms.FieldType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'cms.fieldtype': {
            'Meta': {'object_name': 'FieldType'},
            'field_type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['cms']
