from django import template 

register = template.Library()

def add_js(context, path):
    js_array = context['js_array']
#    if js_array is None:
#        js_array = []
#        context['js_array'] = js_array

#    js_array.append(path)
    print path
    return path
    
#    return JsHolderNode(path)

def render_js_list():
    return JsRendererNode()

class JsHolderNode(template.Node):
    def __init__(self, js_path):
        self.js_path = js_path

    def render(self, context):
        js_array = context['js_array']
        if js_array is None:
            js_array = []
            context['js_array'] = js_array

        js_array.append(self.js_path)
        return ''

class JsRendererNode(template.Node):
    def __init__(self):
        pass

    def render(self, context):
        js_array = context['js_array']
        if js_array is None:
            return ''

        result = ""
        for script in js_array:
            resilt += '<script type="text/javascript" src="'+script+'"></script>'
        return result

register.simple_tag(takes_context=True)(add_js)
