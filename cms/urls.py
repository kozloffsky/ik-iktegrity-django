from django.conf.urls import patterns, url

from cms  import views

urlpatterns = patterns('',
    url(r'^(?P<document_id>\d+)/$', views.view_document, name="document"),
    url(r'^add/', views.add_document, name="add_document")
)
