from django.http import Http404, HttpResponse
from django.shortcuts import render

from cms import models

def view_document(request, document_id):
    try:
        document = models.Document.objects.get(pk=document_id)
    except models.Document.DoesNotExist:
        raise Http404
    return render(request, 'cms/document.html', {'document': document})

def home(request):
    return render(request, 'cms/home.html')

def robots(request):
    return HttpResponse("User-agent: *\nDisallow: /")



def add_document(request):
    """
    show view for adding new document
    """   
    types = models.DocumentType.objects.all()
    return render(request, 'add_document.html', {
        'doc_types': types,
        'form' : DocForm(),
        'fields' : FieldForm()
    })


from django import forms
from django.contrib.admin import widgets

class DocForm(forms.ModelForm):
    class Meta:
        model = models.Document
        fields = ['document_type']
    
class FieldForm(forms.ModelForm):
    class Meta:
        model = models.DateTimeField
        fields = ['value', 'document']

    def __init__(self, *args, **kwargs):
        super(FieldForm, self).__init__(*args, **kwargs)
        self.fields['value'].widget = widgets.AdminDateWidget()


from jsonrpc import jsonrpc_method
from django.core import serializers

@jsonrpc_method("cms.getMenuItems")
def get_manu_items(request):
    return [{"route":"test-item","label":"Test Item"}]

@jsonrpc_method("cms.getDocument")
def get_document(request):
    return serializers.serialize('json', models.Document.objects.all())

@jsonrpc_method("cms.getDocumentTypes")
def get_document_types(request):
    return serializers.serialize('json', models.DocumentType)

import importlib

@jsonrpc_method("cms.findAll")
def model_find_all(request, modelName):
    m = importlib.import_module('cms.models')
    model = getattr(m, modelName)
    return serializers.serialize('json', model.objects.all())


    
