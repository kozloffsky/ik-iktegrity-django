# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'DocumentType.name'
        db.add_column(u'cms_documenttype', 'name',
                      self.gf('django.db.models.fields.CharField')(default=datetime.datetime(2014, 6, 30, 0, 0), max_length=255),
                      keep_default=False)

        # Adding field 'DocumentType.description'
        db.add_column(u'cms_documenttype', 'description',
                      self.gf('django.db.models.fields.CharField')(default=datetime.datetime(2014, 6, 30, 0, 0), max_length=1500),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'DocumentType.name'
        db.delete_column(u'cms_documenttype', 'name')

        # Deleting field 'DocumentType.description'
        db.delete_column(u'cms_documenttype', 'description')


    models = {
        u'cms.document': {
            'Meta': {'object_name': 'Document'},
            'creation_date': ('django.db.models.fields.DateTimeField', [], {}),
            'document_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cms.DocumentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'parent_document': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['cms.Document']"}),
            'update_date': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'cms.documenttype': {
            'Meta': {'object_name': 'DocumentType'},
            'creation_date': ('django.db.models.fields.DateTimeField', [], {}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1500'}),
            'fields': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['cms.FieldType']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'cms.fieldtype': {
            'Meta': {'object_name': 'FieldType'},
            'field_type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['cms']