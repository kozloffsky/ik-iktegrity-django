from django.db import models

class Post(models.Model):
    title = models.CharField(max_length = 255)
    content = models.CharField(max_length = 15000)
    pub_date = models.DateTimeField('date published')

    def __unicode__ (self):
        return self.title
