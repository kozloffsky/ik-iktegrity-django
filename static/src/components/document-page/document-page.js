define(['knockout', 'text!./document-page.html'], function(ko, templateMarkup) {

  function DocumentPage(params) {
    this.message = ko.observable('Hello from the document-page component!');
  }

  // This runs when the component is torn down. Put here any logic necessary to clean up,
  // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
  DocumentPage.prototype.dispose = function() { };
  
  return { viewModel: DocumentPage, template: templateMarkup };

});
