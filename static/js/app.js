
requirejs.config({
    "baseUrl": "/static/js",
    "paths": {
	"app": "app",
	"jquery": "//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min",
	//Knockout
	"ko": "//cdnjs.cloudflare.com/ajax/libs/knockout/3.1.0/knockout-min",
	"jrpc": "https://raw.githubusercontent.com/Textalk/jquery.jsonrpcclient.js/master/jquery.jsonrpcclient"
    }
});


requirejs(["app/main"]);

