define(['knockout','jquery','app/services/rpc', 'app/models/model'], function(ko, j, rpc, model){
    return (function(){
	var documentTypes = ko.observableArray([]);

	var doc = model.factory('DocumentType');
	doc.all();
	var DocumentService = function(){
	    
	}

	DocumentService.prototype.getDocumentTypes = function(){
	    j.when(rpc.call('cms.getDocumentTypes',[]))
		.then(function(result){
		    result = j.parseJSON(result);
		    for (var item in result){
			console.log(item);
			console.log(result[item]);
		    }
		});
	    return documentTypes;
	}

	return new DocumentService();
    })();
});
