from django.db import models
from mptt.models import TreeForeignKey
from mptt.models import MPTTModel

class FieldType(models.Model):
    TYPES = (
        ('INT', 'Ingerer'),
        ('DATE', 'Date'),
        ('TIME', 'Time'),
        ('DATE_TIME', 'DateTime'),
        ('STR', 'Short String'),
        ('TXT', 'Plain Text'),
        ('RTXT', 'Rich Text'),
    )
    name = models.CharField(max_length = 255)
    field_type = models.CharField(max_length = 50, choices=TYPES)

    def __unicode__(self):
        return self.name

class Field(models.Model):
    field_type = models.ForeignKey(FieldType)
    document = models.ForeignKey('Document')

class IntField(Field):
    value = models.IntegerField(default = 0)

class CharField(Field):
    value = models.CharField(max_length = 255)

class DateField(Field):
    value = models.DateField()

class DateTimeField(Field):
    value = models.DateTimeField()



class DocumentType(models.Model):
    """
    """
    creation_date = models.DateTimeField('date created')
    fields = models.ManyToManyField(FieldType)
    name = models.CharField(max_length = 255)
    description = models.CharField(max_length = 1500)

    def __unicode__(self):
        return self.name
    
    
class Document(MPTTModel):
    """
    Page model - entity without any user defined field. only system used fields
    type - type of page
    creation_date - date when page was created
    is_published - is page published to frontend
    parent_page - parent page. if None - then root
    update_date - date when this page was last updated

    get fields d.field_set.objects.all()
    """
    creation_date = models.DateTimeField('date created')
    update_date = models.DateTimeField('date updated')
    is_published = models.BooleanField(default=False)
    document_type = models.ForeignKey(DocumentType)    
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')

    class MPTTMeta:
        order_insertion_by = ['creation_date']
