from django.conf.urls import patterns, include, url
from django.contrib import admin
from jsonrpc import jsonrpc_site

from cms import views



admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', views.home, name='home'),
    url(r'^robots.txt$', views.robots, name="robots"),
    url(r'^blog/', include('blog.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^doc/', include('cms.urls')),
                       
    url(r'^api/browse/$', 'jsonrpc.views.browse', name="jsonrpc_browser"),
    url(r'^api/$', jsonrpc_site.dispatch, name="jsonrpc_mountpoint"),

)
