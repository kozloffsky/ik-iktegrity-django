# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'FieldType'
        db.create_table(u'cms_fieldtype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('field_type', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'cms', ['FieldType'])

        # Adding model 'DocumentType'
        db.create_table(u'cms_documenttype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('creation_date', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'cms', ['DocumentType'])

        # Adding M2M table for field fields on 'DocumentType'
        m2m_table_name = db.shorten_name(u'cms_documenttype_fields')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('documenttype', models.ForeignKey(orm[u'cms.documenttype'], null=False)),
            ('fieldtype', models.ForeignKey(orm[u'cms.fieldtype'], null=False))
        ))
        db.create_unique(m2m_table_name, ['documenttype_id', 'fieldtype_id'])

        # Adding model 'Document'
        db.create_table(u'cms_document', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('creation_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('update_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('is_published', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('document_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cms.DocumentType'])),
            ('parent_document', self.gf('mptt.fields.TreeForeignKey')(blank=True, related_name='children', null=True, to=orm['cms.Document'])),
        ))
        db.send_create_signal(u'cms', ['Document'])


    def backwards(self, orm):
        # Deleting model 'FieldType'
        db.delete_table(u'cms_fieldtype')

        # Deleting model 'DocumentType'
        db.delete_table(u'cms_documenttype')

        # Removing M2M table for field fields on 'DocumentType'
        db.delete_table(db.shorten_name(u'cms_documenttype_fields'))

        # Deleting model 'Document'
        db.delete_table(u'cms_document')


    models = {
        u'cms.document': {
            'Meta': {'object_name': 'Document'},
            'creation_date': ('django.db.models.fields.DateTimeField', [], {}),
            'document_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cms.DocumentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'parent_document': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['cms.Document']"}),
            'update_date': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'cms.documenttype': {
            'Meta': {'object_name': 'DocumentType'},
            'creation_date': ('django.db.models.fields.DateTimeField', [], {}),
            'fields': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['cms.FieldType']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'cms.fieldtype': {
            'Meta': {'object_name': 'FieldType'},
            'field_type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['cms']